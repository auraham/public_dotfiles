# Public dotfiles

Some dotfiles for using on non-personal computers.

## VSCode

Use the following commands to backup your settings:

MacOS:

```
code --list-extensions > ~/git/public_dotfiles/vscode/extensions_macos.txt
cp ~/Library/Application\ Support/Code/User/settings.json ~/git/public_dotfiles/vscode/settings.json.macos
cp ~/Library/Application\ Support/Code/User/keybindings.json ~/git/public_dotfiles/vscode/keybindings.json.macos
```

**Note** For MacOS, add this snippet to `~/.profile`:

```
export PATH="$PATH:/Applications/Visual Studio Code.app/Contents/Resources/app/bin"
```





## References

- [Backup VSCode settings](https://superuser.com/questions/1080682/how-do-i-back-up-my-vs-code-settings-and-list-of-installed-extensions)

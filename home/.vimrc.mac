"set termguicolors       " enable 24 bit colors (load before theme)                              https://stackoverflow.com/questions/64762325/how-to-set-up-nord-vim-color-scheme
"colorscheme onehalfdark " define theme
"colorscheme jellybeans " define theme
"colorscheme codedark " define theme SE PUEDE MEJORAR
"colorscheme aurora " define theme SE PUEDE MEJORAR
colorscheme  jellybeans " define theme SE PUEDE MEJORAR
set tabstop=4
set softtabstop=4       " remove 4 spaces (ie a tab) with a single press of the <backspace> key
set expandtab           " replace tabs with white spaces
set number
set relativenumber
set showcmd
set incsearch
set hlsearch
set nowrap
set title
set ruler               " more info in status bar
set autoindent
set shiftwidth=4        " number of spaces to use for auto ident (this is needed for python)    https://stackoverflow.com/questions/65076/how-do-i-set-up-vim-autoindentation-properly-for-editing-python-files
set splitright          " vertical splits are created on the right rather than left             https://www.youtube.com/watch?v=1xKE62tTYj4
set splitbelow          " horizontal splits are created on the bottom rather than top           https://superuser.com/questions/212257/how-do-i-open-a-blank-new-file-in-a-split-in-vim
set ignorecase          " searching is not case sensitive: 'the' matches 'The', 'the', ...      https://vim.fandom.com/wiki/Searching
set smartcase           " if both ignorecase and smartcase are on, searching case sensitive if the pattern has an uppercase letter      https://vim.fandom.com/wiki/Searching
"set mouse=a             " it allows you to resize splits with the mouse                         https://vi.stackexchange.com/questions/13566/fully-disable-mouse-in-console-vim
set fillchars+=vert:\   " removes | in splits
" Background and SpecialKeys (jellybeans only)
let g:jellybeans_overrides = {
 \   'background': { 'guibg': '252525' },
 \   'SpecialKey': { 'guibg': '252525' },
 \}

" Set :terminal bg color
" https://alvinalexander.com/linux/vi-vim-editor-color-scheme-syntax/
" https://www.reddit.com/r/vim/comments/sa6ypn/change_terminal_color/htsqxz8/
" :hi Terminal ctermbg=166 guibg=#F0A070
hi Terminal ctermbg=Black

" https://github.com/nanotech/jellybeans.vim
" use same color as terminal's bg
" let g:jellybeans_overrides = {
" \   'background': { 'ctermbg': 'none', '256ctermbg': 'none' },
" \}
" if has('termguicolors') && &termguicolors
"     let g:jellybeans_overrides['background']['guibg'] = 'none'
" endif

" Invisible characters
" In insert mode, type ctrl+v, then type the unicode code 
" · u00b7
" ¬ u00ac
" ▸ u25b8 
" https://jdhao.github.io/2020/10/07/nvim_insert_unicode_char/
" http://vimcasts.org/episodes/show-invisibles/
set listchars=tab:▸\ ,eol:¬,space:·,trail:-

" airline
let g:airline_section_z=''
let g:airline#extensions#tabline#enabled = 1

" Move 1 line up or down in normal mode
" . is the line number of the current line
" :[range]m[ove] {address} moves the lines given by [range] to below the line
"                          given by {address}
" :m .+1 means (move line to current line number +1)
" :m .-2 means (move line to current line number -2)
"
" Other options
" :.m .+1   up
" :m+       up
" :m-2      down
" :.m.-2    down
" https://vim.fandom.com/wiki/Moving_lines_up_or_down
nnoremap K :m .-2<CR>
nnoremap J :m .+1<CR>


" Move 1 or more lines up or down in visual selection mode
" '> is a mark assigned by vim to identify the selection end
" :m '>+1 means (move lines to line number of end of selection +1)
" https://vim.fandom.com/wiki/Moving_lines_up_or_down
vnoremap K :m '<-2<CR>gv
vnoremap J :m '>+1<CR>gv

" Soft line break
" set wrap linebreak nolist
" You can also use this command and calling it as follows:
" :Wrap<CR>
" Note: custom commands must start with an uppercase letter
" http://vimcasts.org/episodes/soft-wrapping-text/
" https://vi.stackexchange.com/questions/13768/what-is-the-standard-way-of-creating-new-vim-commands
" http://adp-gmbh.ch/vim/user_commands.html
command! -nargs=* Wrap set wrap linebreak nolist

" organiza por seccion?


" for macos
" set re=2          " better syntax highlighing in macos                                    https://github.com/fatih/vim-go/issues/3171


let &titleold="Terminal"

" https://github.com/elixir-editors/vim-elixir
syntax on
filetype plugin indent on

set cursorline


" mappings
nnoremap gb :ls<CR>:b<Space>

" https://vim.fandom.com/wiki/Jump_to_a_line_number
" In normal mode, press 123 <Enter> to go to line 123
"nnoremap <CR> G


" options for elixir
" https://stackoverflow.com/questions/158968/changing-vim-indentation-behavior-by-file-type
" https://github.com/GrzegorzKozub/vim-elixirls
autocmd FileType elixir,eelixir setlocal autoindent

" options for perl (remote server)
autocmd FileType perl setlocal tabstop=2 softtabstop=2 expandtab

" nim
" https://github.com/zah/nim.vim
fun! JumpToDef()
  if exists("*GotoDefinition_" . &filetype)
    call GotoDefinition_{&filetype}()
  else
    exe "norm! \<C-]>"
  endif
endf

" Jump to tag
nn <M-g> :call JumpToDef()<cr>
ino <M-g> <esc>:call JumpToDef()<cr>i


" not-recommended options

"set autoindent      " automatic indentation
"highlight CursorLine ctermbg=DarkMagenta
